Rust bot powered by [serenity](https://github.com/serenity-rs/serenity) [![Build Status](https://travis-ci.org/Arzte/Arzte-bot.svg?branch=master)](https://travis-ci.org/Arzte/Arzte-bot)

Personal/Public bot, currently in early development, with a focus on being a personal bot for `Arzte the Dream Queen#0001` atm.

## Running arzte

Download the latest release of arzte, then extract the bin file. Copy settings.example.toml to settings.toml, and set your Discord token with your own bot token.

```token = "MTc1MDQ4MTc0MzMzOTg0NzY4.DxQwWQ.7TYu1RMBpUkTFlkxhYHsqR_89Nw"``` (not a real token don't worry)

After setting your bot token you can then start arzte bot, simple run this command ``` ./arzte ```